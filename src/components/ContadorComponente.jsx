import React, { useState, Fragment }from 'react';

// React Hooks usando función flecha 
const ContadorComponente = (props) => {

    // * Crear una constante
    // * Crear un estado en React Hooks
    const [numero, setNumero] = useState(0);

    const fncAumentar = () => {
        setNumero(numero + 1);
        console.log('Me diste un click.');
    }

    return (
    <Fragment>
        <h3>Componente Contador {numero}</h3>
        <button onClick={fncAumentar}>Aumentar</button>
    </Fragment>
    );
}
 
export default ContadorComponente;