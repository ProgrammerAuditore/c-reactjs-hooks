import React, { useState } from 'react';

const Lista = () => {

    const [arrayNumero, setArrayNumero] = useState([1,2,3,4,5,6,7]);
    const [numero, setNumero] = useState(8);

    const agregarItem = () => {
        setNumero(numero + 1);

        console.log('Click...');
        setArrayNumero([ ...arrayNumero, numero]);
    }
    
    return (<>
        <h2>Listar números: </h2>
        <button onClick={agregarItem}>Agregar item</button>
        <ul>
        {
            arrayNumero.map( (item, index) => {
                return <li key={index} >Item { item } - Index { index }</li>
            })
        }
        </ul>
    </>);
}
 
export default Lista;