import React from 'react';
const JsxComponente = (props) => {
    
    const saludo = "Hola JSX";
    const temperatura = 11;
    
    return (<>
        <h1>{ saludo }</h1>
        <h4>
            Hoy hace : 
            {
                temperatura > 10 ? 'Calor' : 'Hace frio'

            }
        </h4>

    </>);
}
 
export default JsxComponente;