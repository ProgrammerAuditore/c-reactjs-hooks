import HolaMundo from './components/HolaMundo';
import ContadorComponente from './components/ContadorComponente';
import ImplApi from './custom_hooks/ImplApi';
import JsxComponente from './jsx/JsxComponente';
import Lista from './jsx/Lista';

function App() {
  return (
    <div>
      <HolaMundo></HolaMundo>
      <ContadorComponente></ContadorComponente>
      <ImplApi></ImplApi>
      <JsxComponente></JsxComponente>
      <Lista></Lista>
    </div>
  );
}

export default App;
