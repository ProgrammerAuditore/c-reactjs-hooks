import { useEffect, useState } from 'react';

// * Crear un hooks personalizado para consumir API
const useApi = (url, _options) => {

    // * Crear estados
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState(null);
    const [error, setError] = useState('');

    // * Crear función para las peticiones
    const doFetch = async (options = _options) => {
        try{
            const response = await fetch(url, options);

            if(!response.ok){
                throw new Error(response.statusText);
            }

            const data = await response.json();
            setData(data);
        } catch (err){
            setError(err.message);
        }

        setIsLoading(false);
    }

    // * Hace que se ejecute una sola vez
    useEffect(() => {
        doFetch();
    }, []);

    // * Regresa estados de Hook
    return {
        response : data,
        error,
        isLoading,
        refetch:  doFetch,
    };


};

export default useApi;