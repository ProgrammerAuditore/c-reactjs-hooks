import useApi from './useApi';

const ImplApi = () => {
    
    // * Obtener los estados de hooks
    const { response, error, isLoading, refetch} = useApi('http://localhost:4444/orders/aassa');

    if(isLoading){
        return <h1>Cargado...</h1>;
    }

    const onRefresh = () => {
        console.log("Consumiendo API...");
        refetch();
    }

    return (<>
        {/* Mostrar un mensaje de error  */}
        {error && (
        <>
          <h1>Error</h1>
          <p>{error}</p>
        </>
        )}

        
        {/* Mostrar los datos si existen */}
        {isLoading && (
            <>
                <h3>Data</h3>
                <ul>
                    {response.map( (order) => (
                        <li key={order.id}>{order.name}</li>
                    ))}
                </ul>
            </>
        )}

        <button onClick={onRefresh}>Refrescar</button>    
    </>);
}

export default ImplApi;